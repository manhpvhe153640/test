﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise12._9
{
    internal class StudentNameComparer : IComparer<Student>
    {
        public int Compare(Student? x, Student? y)
        {
             int a = x.Name.CompareTo(y.Name);
            //if (a == 0)
            //{
            //    return x.Dob.CompareTo(y.Dob);
            //}
            return a;
        }
    }
}
