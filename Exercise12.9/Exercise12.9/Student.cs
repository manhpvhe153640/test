﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise12._9
{
    internal class Student : IComparable<Student>
    {
        public int Code { get; set; }
        public string Name { get; set; }    
        public DateTime Dob { get; set; }  
        public string Major { get; set; }

        public Student()
        {
        }

        public Student(int code, string name, DateTime date, string major)
        {
            Code = code;
            Name = name;
            Dob = date;
            Major = major;
        }


        public override string? ToString()
        {
            return $"Student: {Code}, {Name}, {String.Format("{0:M/d/yyyy}", Dob)}, {Major}";
        }

        public void Input()
        {
            while (true)
            {
                try
                {
                    Code = Inputer.GetInt32("Code:", 0, Int32.MaxValue);
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Re-input:");
                }
            }

            while (true)
            {
                try
                {
                    Name = Inputer.GetString("Name:", "[a-z A-Z]+");
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Re-input:");
                }
            }

            while (true)
            {
                try
                {
                    Dob = Inputer.GetDateTime("Dob", 17, 60);
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Re-input:");
                }
            }

            while (true)
            {
                try
                {
                    Major = Inputer.GetString("Major:");
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Re-input:");
                }
            }
        }

        public void ReadDataFromFile(String line) {
            string [] part = line.Split("|");
            Code = Convert.ToInt32(part[0]);
            Name = part[1];
            Dob = Convert.ToDateTime(part[2]);
            Major = part[3];
        }

        public override bool Equals(object? obj)
        {
            return obj is Student student &&
                   Code == student.Code;
        }
        //using IComparable
        public int CompareTo(Student other)
        {
            return this.Code.CompareTo(other.Code);
        }
    }
}
