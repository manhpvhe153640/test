﻿using Exercise12._9;

internal class Program
{
    private static void Main(string[] args)
    {
        //CourseWithGPA courseWithGPA = new CourseWithGPA();
        //courseWithGPA.Input();
        //courseWithGPA.Display();
        //Course course = new Course();
        //course.Input();
        //course.Display();

        Course course = new Course();
        course.ReadDataFromFile(@"F:\PRN211\Exercise12.9\Exercise12.9\TextFile1.txt");
        course.Display();
        Console.WriteLine("--------------");
        //CourseWithGPA courseWithGPA = new CourseWithGPA();
        //courseWithGPA.ReadDataFromFile(@"F:\PRN211\Exercise12.9\Exercise12.9\TextFile1.txt");
        //courseWithGPA.Display();
        Console.WriteLine("Sort by code");
        course.SortByCode();
        course.Display();
        Console.WriteLine("--------------");
        Console.WriteLine("Sort by name");
        course.SortByName();
        course.Display();
        Console.WriteLine("--------------");
        Console.WriteLine("Sort by DOB");
        course.SortByDOB();
        course.Display();
        Console.WriteLine("--------------");
        Console.WriteLine("Sort by DOB and major");
        course.SortByDOBAndMajor();
        course.Display();

    }
}