﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Exercise12._9
{
    internal class Course
    {
        public string Code { get; set; }
        public string Subject { get; set; }
        List<Student> students;

        public Course()
        {
            students = new List<Student>();
        }

        public Course(string code, string subject)
        {
            students = new List<Student>();
            Code = code;
            Subject = subject;
        }

        public void AddStudent(Student s)
        {
            //foreach (Student item in students)
            //    if (s.Equal(item)) return;
            if (!students.Contains(s))
                students.Add(s);
        }

        public void Input()
        {
            Code = Inputer.GetString("Course Code:");
            Subject = Inputer.GetString("Subject:");
            while (true)
            {
                string cont = Inputer.GetString("Input student? Y - Yes, N - No.");
                if (cont.ToUpper().Equals("Y"))
                {
                    Student s = new Student();
                    s.Input();
                    AddStudent(s);
                }
                else break;
            }
        }

        public void ReadDataFromFile(string fileName) {
            StreamReader streamReader = new StreamReader(fileName);
            Course course = new Course();

            string line;
            int a = 1;
            while ((line = streamReader.ReadLine()) != null)
            {
                if (a == 1)
                {
                    Code = line;
                }
                else if (a == 2)
                {
                    Subject = line;
                }
                else {
                    Student s = new Student();
                    s.ReadDataFromFile(line);
                    students.Add(s);
                }
                a++;

            }
        }

        //using IComparable
        public void SortByCode() {
            students.Sort();
        }
        //using IComparer
        public void SortByName()
        {
            students.Sort(new StudentNameComparer());
        }
        //using Comparison
        
        public void SortByDOB() { 
            students.Sort((e1,e2) => e1.Dob.CompareTo(e2.Dob));
        }
        //using Comparision
        public int compareByDOBAndMajor(Student x, Student y)
        {
            int a= x.Dob.CompareTo(y.Dob);
            if (a == 0)
            {
                return x.Major.CompareTo(y.Major);
            }
            return a;
        }
        public void SortByDOBAndMajor() {
            students.Sort(compareByDOBAndMajor);
        }
        public void Display()
        {
            Console.WriteLine($"Course: {Code} - {Subject}");
            Console.WriteLine("List of students:");
            foreach (Student s in students)
                Console.WriteLine(s);
        }
    }
}
