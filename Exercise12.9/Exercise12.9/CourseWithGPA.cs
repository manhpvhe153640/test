﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise12._9
{
    internal class CourseWithGPA
    {
        public string Code { get; set; }
        public string Subject { get; set; }
        Dictionary<Student, Double> students;

        public CourseWithGPA()
        {
            students = new Dictionary<Student, Double>();
        }

        public CourseWithGPA(string code, string subject)
        {
            students = new Dictionary<Student, Double>();
            Code = code;
            Subject = subject;
        }

        public void AddStudent(Student s, double GPA = 0)
        {
            foreach (Student item in students.Keys)
                if (s.Equals(item)) return;              
            students.Add(s, GPA);



            //if (!students.ContainsKey(s))
            //    students.Add(s, GPA);
        }

        public void Input()
        {
            Code = Inputer.GetString("Course Code:");
            Subject = Inputer.GetString("Subject:");
            while (true)
            {
                string cont = Inputer.GetString("Input student? Y - Yes, N - No.");
                if (cont.ToUpper().Equals("Y"))
                {
                    Student s = new Student();
                    s.Input();
                    string cont1 = Inputer.GetString("Input gpa student? Y - Yes, N - No.");
                    if (cont1.ToUpper().Equals("Y"))
                    {
                        double gpa = Inputer.GetDouble("GPA:",0,double.MaxValue);
                        AddStudent(s,gpa);
                    }
                    else {
                        AddStudent(s);
                    }
                    
                }
                else break;
            }
        }

        public void ReadDataFromFile(string fileName)
        {
            StreamReader streamReader = new StreamReader(fileName);
            Course course = new Course();

            string line;
            int a = 1;
            while ((line = streamReader.ReadLine()) != null)
            {
                if (a == 1)
                {
                    Code = line;
                }
                else if (a == 2)
                {
                    Subject = line;
                }
                else
                {
                    Student s = new Student();
                    s.ReadDataFromFile(line);
                    int lastIndex = line.LastIndexOf("|");
                    students.Add(s,Convert.ToDouble(line.Substring(lastIndex+1)));
                }
                a++;

            }
        }

        public void Display()
        {
            Console.WriteLine($"Course: {Code} - {Subject}");
            Console.WriteLine("List of students:");
            foreach (Student s in students.Keys)
                Console.WriteLine($"{s} - GPA: {students[s]}");
        }
    }
}
