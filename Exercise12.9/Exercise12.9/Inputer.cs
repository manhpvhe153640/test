﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Exercise12._9
{
    internal class Inputer
    {
        public static int GetInt32(string mess, int min, int max)
        {
            Console.WriteLine(mess);
            int k = Convert.ToInt32(Console.ReadLine());
            if (k < min || k > max)
                throw new OverflowException("Du lieu nhap vao khong thoa man min - max");
            return k;
        }

        public static double GetDouble(string mess, double min, double max)
        {
            Console.WriteLine(mess);
            Double k = Convert.ToDouble(Console.ReadLine());
            if (k < min || k > max)
                throw new OverflowException("Du lieu nhap vao khong thoa man min - max");
            return k;
        }

        public static DateTime GetDateTime(string mess, DateTime min, DateTime max)
        {
            Console.WriteLine(mess);
            DateTime k = Convert.ToDateTime(Console.ReadLine());
            if (k < min || k > max)
                throw new OverflowException("Du lieu nhap vao khong thoa man min - max");
            return k;
        }

        public static DateTime GetDateTime(string mess, int min, int max)
        {
            Console.WriteLine(mess);
            DateTime k = Convert.ToDateTime(Console.ReadLine());
            int differ = DateTime.Now.Year - k.Year; //co the them logic de kiem tra chi tiet den tung ngay - sv tu them vao. 
            if (differ < min || differ > max)
                throw new OverflowException("Du lieu nhap vao khong thoa man min - max");
            return k;
        }

        public static String GetString(string mess, string pattern = null)
        {
            Console.WriteLine(mess);
            string? s = Console.ReadLine();
            if (pattern is null) return s;
            Regex regex = new Regex(pattern);
            if (!regex.IsMatch(s))
                throw new OverflowException("Du lieu nhap vao khong thoa man pattern: " + pattern);
            return s;
        }
    }
}
