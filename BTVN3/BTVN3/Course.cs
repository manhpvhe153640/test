﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BTVN3
{
    internal class Course
    {
        
       
        
        public string Code { get; set; }
        public string Subject { get; set; }
        private List<Student> Students = new List<Student>();
        public Course()
        {
        }
        public Course(string code, string subject, List<Student> students)
        {
            Code = code;
            Subject = subject;
            Students = students;
        }
       
        public List<Student> GetStudents() { 
            return Students;
        }
        public void AddStudent(Student e)
        {               
            Students.Add(e);
        }
        public void Input()
        {
            Console.WriteLine("Enter code: ");
            Code = Console.ReadLine();
            Console.WriteLine("Enter Subject: ");
            Subject = Console.ReadLine();
        }
        public void Display() {
            Console.WriteLine($"Code: {Code}");
            Console.WriteLine($"Subject: {Subject}");
            Console.WriteLine($"List Student:");
            
            if(Students != null) {
                int a = 1;
                foreach (Student student in Students)
                {
                    Console.WriteLine("Student "+a+":");
                    Console.WriteLine(student.Name);
                    a++;
                }
            }
            else { 
                Console.WriteLine("List student empty"); 
            }
           
        }
    }
}
