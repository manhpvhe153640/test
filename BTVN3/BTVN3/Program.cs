﻿using BTVN3;

internal class Program
{
    private static void Main(string[] args)
    {
       Course course = new Course();
        course.Input();
        while (true) {
            try {
                Student student = new Student();
                student.Input(course.GetStudents());
                course.AddStudent(student);
                break;
            }
            catch (StudentFormatException e) {
                Console.WriteLine(e.Message); 
            }
            
        }                  
        course.Display();


    }
}