﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BTVN31
{
    public class Student
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }

        public string Major { get; set; }

        public Student()
        {
        }

        public Student(string code, string name, DateTime dob, string major)
        {
            Code = code;
            Name = name;
            Dob = dob;
            Major = major;
        }

        private void Display() {
            Console.WriteLine($"Code: {Code}, Name: {Name}, Dob: {Dob}, Major: {Major}");
        }

        public void Input(Dictionary<Student,Double> a) {
            Console.WriteLine("Enter code: ");
            Code = Console.ReadLine();
            if (!CheckIdExist(Code, a)) {
                StudentFormatException e = new StudentFormatException("Student code is already");
                throw e;
            }
            if (!Regex.IsMatch(Code, "^[a-zA-Z]{2}[\\d]{6}$"))
            {
                StudentFormatException e = new StudentFormatException("Format Code wrong");
                throw e;
            }
            Console.WriteLine("Enter name: ");
            Name = Console.ReadLine();
            if (!Regex.IsMatch(Name, "^[a-zA-Z\\s]{1,}$")||Name.Trim().Equals("")) {
                StudentFormatException e = new StudentFormatException("Format Name wrong");
                throw e;
            }
            Console.WriteLine("Enter Dob(MM/dd/yyyy): ");
            Dob = Convert.ToDateTime(Console.ReadLine());
            DateTime dateNow = DateTime.Now;
            TimeSpan interval = dateNow.Subtract(Dob);
            if (interval.Days > 21900 || interval.Days < 6205) {
                StudentFormatException e = new StudentFormatException("Date invalid");
                throw e;
            }
            Console.WriteLine("Major: ");
            Major = Console.ReadLine();
            if (!Major.ToUpper().Equals("SE") && !Major.ToUpper().Equals("IA") && !Major.ToUpper().Equals("AI") && !Major.ToUpper().Equals("GD") && !Major.ToUpper().Equals("SB") && !Major.ToUpper().Equals("IOT")) {
                StudentFormatException e = new StudentFormatException("Major not exist");
                throw e;
            }



        
        }

        public static Boolean CheckIdExist(string id, Dictionary<Student,Double> list) 
        {
            if (list != null) {
                foreach (Student student in list.Keys)
                {
                    if (student.Code.ToLower().Equals(id.ToLower()))
                    {
                        return false;
                    }
                }
            }           
            return true;
        }

        public override string? ToString()
        {
            return $"Code: {Code}, Name: {Name}, Dob: {Dob}";
        }
    }
}
