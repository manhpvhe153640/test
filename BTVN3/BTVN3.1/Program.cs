﻿using BTVN31;

internal class Program
{
    private static void Main(string[] args)
    {
        Course course = new Course();
        course.Input();
        Student student1 = new Student();
        Student student2 = new Student();
        while (true)
        {
            try {
                student1.Input(course.getListStudent());
                course.AddStudent(student1);              
                break;
            }
            catch(StudentFormatException e)
            {
                Console.WriteLine(e.Message);
            }
            
        }
        course.Display();
        while (true)
        {
            try
            {               
                student2.Input(course.getListStudent());
                course.AddStudent(student2, 9.2);
                break;
            }
            catch (StudentFormatException e)
            {
                Console.WriteLine(e.Message);
            }

        }


        course.Display();
        

    }
}