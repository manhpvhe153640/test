﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BTVN31
{
    internal class Course
    {
        public string Code { get; set; }
        public string Subject { get; set; }

        private Dictionary<Student, Double> listStudent = new Dictionary<Student, double>();
        public Course()
        {
        }
        public Course(string code, string subject, Dictionary<Student, double> listStudent)
        {
            Code = code;
            this.Subject = subject;
            this.listStudent = listStudent;
        }
        public Dictionary<Student, Double> getListStudent(){
            return listStudent;
        }
        public void Input()
        {
            Console.WriteLine("Enter code: ");
            Code = Console.ReadLine();
            Console.WriteLine("Enter Subject: ");
            Subject = Console.ReadLine();
        }
        public void AddStudent(Student e) { 
            listStudent.Add(e, 0);
        }
        public void AddStudent(Student e,double b)
        {
            listStudent.Add(e, b);
        }
        public void Display()
        {
            Console.WriteLine($"Code: {Code}");
            Console.WriteLine($"Subject: {Subject}");
            Console.WriteLine($"List Student:");

            if (listStudent != null)
            {
                int a = 1;
                foreach (var list in listStudent)
                {
                    Console.WriteLine(list.Key.Name);
                    Console.WriteLine(list.Value);
                }
            }
            else
            {
                Console.WriteLine("List student empty");
            }

        }
    }
}
