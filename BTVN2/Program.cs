﻿using BTVN2;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
        Student student = new Student();
        while (true) {
            try
            {
                student.Input();
                break;
            }
            catch (StudentFormatException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        student.Display();
        Console.WriteLine("add");
    }
}