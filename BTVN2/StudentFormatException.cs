﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTVN2
{
    internal class StudentFormatException : Exception
    {
        public StudentFormatException(string? message) : base(message)
        {
        }
    }
}
