﻿internal class Program
{
    public long folderSize(DirectoryInfo dir)
    {
        long result = 0;
        FileInfo[] fiArr = dir.GetFiles();
        for (int i = 0; i < fiArr.Length; i++)
        {
            result = result + fiArr[i].Length;
        }
        DirectoryInfo[] diArr = dir.GetDirectories();
        for (int i = 0; i < diArr.Length; i++)
        {
            result = result + folderSize(diArr[i]);
        }
        return result;
    }


    private static void Main(string[] args)
    {
        Console.WriteLine("Nhap duong dan:");
        String path = Console.ReadLine();
        DirectoryInfo directoryInfo = new DirectoryInfo($@"{path}");
        FileInfo files = new FileInfo($@"{path}");
        DriveInfo d = new DriveInfo($@"{path}");

       

        if (directoryInfo.Exists)
        {
            Console.WriteLine("Nhan o cung: {0}", d.VolumeLabel);
            Console.WriteLine("Dinh dang O cung: {0}", d.DriveFormat);
            Console.WriteLine("Thu muc goc cua o cung: {0}", d.RootDirectory);
            Console.WriteLine("Tong dung luong o cung :            {0, 15} bytes ", d.TotalSize);
            Console.WriteLine("Tong dung luong free cua o cung:          {0, 15} bytes", d.TotalFreeSpace);
            Console.WriteLine("Tong dung luong free cua o cung san sang su dung:{0, 15} bytes", d.AvailableFreeSpace);

        }
        if (directoryInfo.Exists)
        {
            //Console.WriteLine($"Kich thuoc: {directoryInfo}");
            string[] DirectoryList = Directory.GetDirectories($@"{path}", "*", SearchOption.AllDirectories);
            Console.WriteLine($"D/S cac foder:");
            foreach (var d1 in DirectoryList) {
                Console.WriteLine($"{d1}");              
            }
            Console.WriteLine($"Tong D/S cac foder: {DirectoryList.Length}");
            string[] listFile = Directory.GetFiles($@"{path}", "*", SearchOption.AllDirectories);        
                Console.WriteLine($"D/S cac file");
                foreach (var info in listFile)
                {
                    Console.WriteLine(info);
                }       
            Console.WriteLine($"Tong so luong file: {listFile.Length}");
        }else if (files.Exists)
        {
            Console.WriteLine($"Kich thuoc: {files.Length.ToString()}");
            Console.WriteLine("Ten file: " + Path.GetFileNameWithoutExtension(path));
            Console.WriteLine("Loai file: " + Path.GetExtension(path));
        }
       
     
    }
}