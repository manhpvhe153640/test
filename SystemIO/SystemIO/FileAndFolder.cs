﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork16_9
{
    internal class FileAndFolder
    {
        public FileAndFolder() { }
        public long folderSize(DirectoryInfo dir)
        {
            long result=0;
            FileInfo[] fiArr = dir.GetFiles();
            for (int i = 0; i < fiArr.Length; i++)
            {
                result = result+ fiArr[i].Length;
            }
            DirectoryInfo[] diArr = dir.GetDirectories();
            for (int i = 0; i < diArr.Length; i++)
            {
                result = result + folderSize(diArr[i]);
            }
            return result;
        }
        public void Process(string path)
        {
            //check xem co phai thu muc hay k.
            if (File.Exists(path))
            {
                FileInfo fileInfo = new FileInfo(path);
                Console.WriteLine("Day la file");
                Console.WriteLine("Kich thuoc file: " + fileInfo.Length +" byte");
                //Console.WriteLine("Ten file: " + fileInfo.Name);
                Console.WriteLine("Ten file: " + Path.GetFileNameWithoutExtension(path));
                Console.WriteLine("Loai file: "+ Path.GetExtension(path));

            }
            if (Directory.Exists(path))
            {   
                DirectoryInfo dir = new DirectoryInfo(path);
                Console.WriteLine("Day la folder");
                DirectoryInfo[] diArr = dir.GetDirectories();
                Console.WriteLine("Kich thuoc: "+folderSize(dir));
                Console.WriteLine("Danh sach cac subfolder:");
                for (int i = 0; i < diArr.Length; i++)
                {
                    Console.WriteLine("    "+diArr[i].Name);
                }
                FileInfo[] fiArr = dir.GetFiles();
                Console.WriteLine("Danh sach cac file:");
                for (int i = 0; i < fiArr.Length; i++)
                {
                    Console.WriteLine("    "+fiArr[i].Name);
                }
            }
        }
    }
}
